#! /usr/bin/env bash
set -e
# set -x

./pre-start.sh

[[ $HOST > 0 ]] && HOST=$HOST || HOST="0.0.0.0"
[[ $PORT > 0 ]] && PORT=$PORT || PORT="8000"

runUvicorn () {
    exec uvicorn app.main:app --port "$PORT" --host "$HOST" "$@" ;
}

devmodeCheck () {
    echo
    [[ $APP_DEBUG == [Tt]rue ]] && APP_DEBUG="True" || APP_DEBUG="False" &&  MODE=$APP_DEBUG ; 
    echo "Developer mode is $MODE"
    echo
}


devmodeCheck

[[ $APP_DEBUG == [Tt]rue ]] && runUvicorn --reload || runUvicorn