from uuid import UUID
import uuid
from pydantic import BaseModel, ConfigDict
from fastapi_users import schemas


class UserRead(schemas.BaseUser[uuid.UUID]):
    model_config = ConfigDict(from_attributes=True)

    username: str | None = None


class UserCreate(schemas.BaseUserCreate):
    model_config = ConfigDict(from_attributes=True)

    username: str | None = None


class UserUpdate(schemas.BaseUserUpdate):
    model_config = ConfigDict(from_attributes=True)

    username: str | None = None


class PasswordCreate(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    password: str | None = None
    comment: str | None = None


class PasswordBase(PasswordCreate):
    password_id: int | None = None
    user_uid: UUID | None = None
