from fastapi_users_db_sqlalchemy import SQLAlchemyBaseUserTableUUID
from sqlalchemy import ForeignKey
from uuid import UUID, uuid4
from sqlalchemy.orm import (
    DeclarativeBase,
    mapped_column,
    Mapped,
)


# Базовый класс для моделей SQLAlchemy. Создается для централизованной настройки.
class Base(DeclarativeBase):
    pass


# Класс пользователей, на нем будет завязана логика аутентификации и авторизации.
class User(SQLAlchemyBaseUserTableUUID, Base):
    __tablename__ = "app_user"
    id: Mapped[UUID] = mapped_column(name="id", primary_key=True, insert_default=uuid4)
    username: Mapped[str] = mapped_column(unique=True, index=True)
    email: Mapped[str] = mapped_column(nullable=False, unique=True, index=True)
    hashed_password: Mapped[str] = mapped_column(nullable=False)
    is_active: Mapped[bool] = mapped_column(nullable=False, default=True)
    is_superuser: Mapped[bool] = mapped_column(nullable=False, default=False)
    is_verified: Mapped[bool] = mapped_column(nullable=False, default=False)


# Класс для хранения паролей, сгенерированных пользователями.
class Password(Base):
    __tablename__ = "app_password"

    password_id: Mapped[int] = mapped_column(
        name="id", primary_key=True, autoincrement=True
    )
    user_uid: Mapped[UUID] = mapped_column(ForeignKey(User.id))
    password: Mapped[str] = mapped_column(
        nullable=False, comment="Сохраненный сгенерированный пароль"
    )
    comment: Mapped[str] = mapped_column(
        nullable=False, comment="Комментарий для сохраненного пароля"
    )
