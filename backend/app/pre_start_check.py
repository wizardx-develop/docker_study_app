import asyncio
from sqlalchemy.ext.asyncio import (
    create_async_engine,
    AsyncEngine,
    async_sessionmaker,
)
from sqlalchemy import select
from appconfig import DB_URL
from tenacity import after_log, before_log, retry, stop_after_attempt, wait_fixed

import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

max_tries = 60 * 5  # 5 minutes
wait_seconds = 1


@retry(
    stop=stop_after_attempt(max_tries),
    wait=wait_fixed(wait_seconds),
    before=before_log(logger, logging.INFO),
    after=after_log(logger, logging.WARN),
)
async def init(engine: AsyncEngine) -> None:
    async_session_maker = async_sessionmaker(engine)
    try:
        async with async_session_maker() as a_seesion:
            await a_seesion.execute(select(1))
    except Exception as e:
        logger.error(e)
        raise e


if __name__ == "__main__":
    engine = create_async_engine(DB_URL)
    asyncio.run(init(engine=engine))
