import uuid
from fastapi_users import FastAPIUsers

from app import models
from app.login.authentication import get_user_manager, auth_backend


router = FastAPIUsers[models.User, uuid.UUID](
    get_user_manager=get_user_manager,
    auth_backends=[auth_backend],
)
