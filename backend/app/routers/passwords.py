from typing import Annotated
from fastapi import Depends, Query, status
from fastapi.routing import APIRouter
from sqlalchemy.ext.asyncio import AsyncSession

from app import models, shemas
from app.db import crud, conn
from app import deps


router = APIRouter()


@router.post("/", status_code=status.HTTP_204_NO_CONTENT)
async def add_password_to_user(
    password: shemas.PasswordCreate,
    user: models.User = Depends(deps.current_active_user),
    db_conn: AsyncSession = Depends(conn.get_async_session),
):
    await crud.password_create(user=user, password=password, db_conn=db_conn)


@router.get("/")
async def get_all_user_passwords(
    skip: Annotated[int | None, Query(ge=0)] = None,
    limit: Annotated[int | None, Query(ge=1, le=100)] = None,
    db_conn: AsyncSession = Depends(conn.get_async_session),
    user: models.User = Depends(deps.current_active_user),
):
    passwords_list = await crud.passwords_get_all(
        user=user, offset=skip, limit=limit, db_conn=db_conn
    )
    return {"Passwords": passwords_list}


@router.put("/", status_code=status.HTTP_204_NO_CONTENT)
async def change_passwod_information(
    password_id: int,
    password: shemas.PasswordCreate,
    user: models.User = Depends(deps.current_active_user),
    db_conn: AsyncSession = Depends(conn.get_async_session),
):
    await crud.password_update(
        user=user,
        password_id=password_id,
        password=password,
        db_conn=db_conn,
    )
    return {"Message": "Password updated!"}


@router.delete("/", status_code=status.HTTP_204_NO_CONTENT)
async def delete_password_by_id(
    id: Annotated[int, Query()],
    db_conn: AsyncSession = Depends(conn.get_async_session),
    user: models.User = Depends(deps.current_active_user),
):
    await crud.password_delete(user=user, id=id, db_conn=db_conn)
    return {"Message": "Deleted!"}
