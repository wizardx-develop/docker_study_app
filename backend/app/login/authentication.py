import uuid
from fastapi import Depends, Request
import redis.asyncio
from fastapi_users.authentication import RedisStrategy
from fastapi_users.authentication import (
    AuthenticationBackend,
    CookieTransport,
)
from fastapi_users import (
    BaseUserManager,
    FastAPIUsers,
    InvalidPasswordException,
    UUIDIDMixin,
)

from app import models, appconfig, shemas
from app.db import conn


REDIS_URL = "redis://{}:{}".format(
    appconfig.base_settings_redis.REDIS_HOST, appconfig.base_settings_redis.REDIS_PORT
)

redis = redis.asyncio.from_url("redis://localhost:6379", decode_responses=True)


SECRET = appconfig.secrets.RSA_SECRET_CODE

cookie_transport = CookieTransport(cookie_max_age=3600)


def get_redis_strategy() -> RedisStrategy:
    return RedisStrategy(redis=redis, lifetime_seconds=3600)


auth_backend = AuthenticationBackend(
    name="cookie",
    transport=cookie_transport,
    get_strategy=get_redis_strategy,
)


class UserManager(UUIDIDMixin, BaseUserManager[models.User, uuid.UUID]):
    reset_password_token_secret = SECRET
    verification_token_secret = SECRET

    async def validate_password(
        self,
        password: str,
        user: shemas.UserCreate | models.User,
    ) -> None:
        if len(password) < 8:
            raise InvalidPasswordException(
                reason="Password should be at least 8 characters"
            )

    async def on_after_register(
        self, user: models.User, request: Request | None = None
    ):
        print(f"User {user.id} has registered.")

    async def on_after_forgot_password(
        self, user: models.User, token: str, request: Request | None = None
    ):
        print(f"User {user.id} has forgot their password. Reset token: {token}")

    async def on_after_request_verify(
        self, user: models.User, token: str, request: Request | None = None
    ):
        print(f"Verification requested for user {user.id}. Verification token: {token}")


async def get_user_manager(user_db=Depends(conn.get_user_db)):
    yield UserManager(user_db)


fastapi_users = FastAPIUsers[models.User, uuid.UUID](get_user_manager, [auth_backend])

current_active_user = fastapi_users.current_user(active=True)
