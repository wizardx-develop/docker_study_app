from pydantic_settings import BaseSettings, SettingsConfigDict


class DatabaseSettings(BaseSettings):
    model_config = SettingsConfigDict(env_file="postgres.env")

    POSTGRES_USER: str
    POSTGRES_PASSWORD: str
    DB_DRIVER: str
    DB_ADDRESS: str
    DB_PORT: str
    DB_NAME: str


class RedisSettings(BaseSettings):
    model_config = SettingsConfigDict(env_file="redis.env")

    REDIS_HOST: str
    REDIS_PORT: int
    REDIS_DECODE_RESPONSES: bool


class AnySettings(BaseSettings):
    favicon_path: str = "app/icons/favicon.ico"


class SecretsSettings(BaseSettings):
    model_config = SettingsConfigDict(env_file="secrets.env")

    RSA_SECRET_CODE: str
    PUBLIC_KEY_PATH: str
    PRIVATE_KEY_PATH: str


base_settings_db = DatabaseSettings()
base_settings_redis = RedisSettings()
secrets = SecretsSettings()


DB_URL: str = "postgresql+{}://{}:{}@{}:{}/{}".format(
    base_settings_db.DB_DRIVER,
    base_settings_db.POSTGRES_USER,
    base_settings_db.POSTGRES_PASSWORD,
    base_settings_db.DB_ADDRESS,
    base_settings_db.DB_PORT,
    base_settings_db.DB_NAME,
)
