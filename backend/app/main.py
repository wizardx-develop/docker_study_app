from fastapi import FastAPI

from app.routers.passwords import router as passwords_router
from app.routers.auth import router as users_router
from app.login.authentication import auth_backend
from app.shemas import UserCreate, UserRead, UserUpdate

app = FastAPI()


app.include_router(
    users_router.get_auth_router(auth_backend),
    prefix="/auth",
    tags=["auth"],
)
app.include_router(
    users_router.get_register_router(UserRead, UserCreate),
    prefix="/auth",
    tags=["auth"],
)
app.include_router(
    users_router.get_reset_password_router(),
    prefix="/auth",
    tags=["auth"],
)
app.include_router(
    users_router.get_verify_router(UserRead),
    prefix="/auth",
    tags=["auth"],
)
app.include_router(
    users_router.get_users_router(UserRead, UserUpdate),
    prefix="/users",
    tags=["users"],
)

app.include_router(
    router=passwords_router,
    # dependencies=[Depends(deps.current_active_user)],
    prefix="/passwords",
    tags=["Vault"],
)
