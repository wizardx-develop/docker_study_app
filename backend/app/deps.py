# Файл с зависимостями
from passlib.hash import pbkdf2_sha512
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
from Crypto.IO import PEM
import base64

from app import appconfig
from app.routers.auth import router as users_router


def hash_password(entered_password: str) -> str:
    return pbkdf2_sha512.hash(entered_password)


def verify_password(password: str, hashed_password: str):
    return pbkdf2_sha512.verify(password, hashed_password)


def encrypt_password(
    entered_password: str, public_key_path: str = appconfig.secrets.PUBLIC_KEY_PATH
):
    UTF8 = "utf-8"
    with open(public_key_path, "rb") as key:
        public_key = RSA.import_key(key.read())
    cipher = PKCS1_OAEP.new(public_key)
    encrypted_password = base64.b64encode(
        cipher.encrypt(entered_password.encode(UTF8))
    ).decode(UTF8)
    return encrypted_password


def decrypt_password(
    encrypted_password: str,
    private_key_path: str = appconfig.secrets.PRIVATE_KEY_PATH,
    SECRET_CODE=appconfig.secrets.RSA_SECRET_CODE,
):
    UTF8 = "utf-8"
    with open(private_key_path, "r") as key_file:
        key_data = key_file.read()
        key_data = PEM.decode(key_data, SECRET_CODE.encode(UTF8))
        private_key = RSA.import_key(key_data[0], passphrase=SECRET_CODE)
    decrypt_cipher = PKCS1_OAEP.new(private_key)
    decrypted_password = decrypt_cipher.decrypt(
        base64.b64decode(encrypted_password)
    ).decode(UTF8)
    return decrypted_password


current_active_user = users_router.current_user(active=True)
