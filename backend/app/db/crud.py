from fastapi.logger import logger
from sqlalchemy import select, update
from sqlalchemy.ext.asyncio import AsyncSession

from app import shemas, deps, models


def create_password_shemas(password: models.Password):
    # Создание схемы pydantic из модели БД
    return shemas.PasswordBase.model_validate(password)


def create_password_model(password: shemas.PasswordBase):
    # Создание модели БД из схемы pydantic
    password_db = models.Password(
        **password.model_dump(exclude=("password_id", "password"))
    )
    password_db.password = deps.encrypt_password(password.password)
    return password_db


def user_read_one_by_name():
    pass


async def password_create(
    user: models.User,
    password: shemas.PasswordBase,
    db_conn: AsyncSession,
):
    # Создание нового пароля
    try:
        password_to_db = create_password_model(password)
        password_to_db.user_uid = user.id
        db_conn.add(password_to_db)
        await db_conn.commit()
    except Exception as ex:
        logger.exception("Database error %s", ex)
        await db_conn.rollback()


async def password_get_one_by_id(
    user: models.User, password_id: int, db_conn: AsyncSession
):
    # Получаем пользователя для получения UID
    db_passwords = await db_conn.execute(
        select(models.Password)
        .filter(models.Password.user_uid == user.id)
        .filter(models.Password.password_id == password_id)
    )
    db_passwords = db_passwords.scalar_one_or_none()
    return db_passwords


async def password_update(
    user: models.User,
    password_id: int,
    password: shemas.PasswordBase,
    db_conn: AsyncSession,
):

    try:
        update_data = password.model_dump(exclude_unset=True)
        update_data["password"] = deps.encrypt_password(update_data["password"])
        s = await db_conn.execute(
            update(models.Password)
            .where(models.Password.password_id == password_id)
            .where(models.Password.user_uid == user.id)
            .values(**update_data)
        )
        await db_conn.commit()
    except Exception as ex:
        logger.exception("Database error %s", ex)
        await db_conn.rollback()


async def passwords_get_all(
    user: models.User, limit: int, offset: int, db_conn: AsyncSession
):
    # Получение всех паролей пользователя
    db_passwords = await db_conn.execute(
        select(models.Password)
        .filter(models.Password.user_uid == user.id)
        .offset(offset)
        .limit(limit)
    )
    db_passwords = db_passwords.scalars().all()
    db_passwords = list(map(create_password_shemas, db_passwords))
    for password in db_passwords:
        password.password = deps.decrypt_password(password.password)
    return db_passwords


async def password_delete(user: models.User, id: int, db_conn: AsyncSession):
    # Удаление пароля
    password_from_db = await db_conn.execute(
        select(models.Password)
        .filter(models.Password.password_id == id)
        .filter(models.Password.user_uid == user.id)
    )
    password_from_db = password_from_db.scalar_one_or_none()
    if password_from_db:
        try:
            await db_conn.delete(password_from_db)
            await db_conn.commit()
        except Exception as ex:
            logger.exception("Database error %s", ex)
            await db_conn.rollback()
