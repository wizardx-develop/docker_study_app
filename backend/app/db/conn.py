# Модуль для создания соединения с базой данных postgresql и с redis
from fastapi import Depends
from sqlalchemy.ext.asyncio import AsyncSession, async_sessionmaker, create_async_engine
from typing import AsyncGenerator
from aioredis import Redis, from_url
from fastapi_users_db_sqlalchemy import SQLAlchemyUserDatabase

from app import appconfig, models


ENGINE = create_async_engine(appconfig.DB_URL)


async def get_async_session() -> AsyncGenerator[AsyncSession, None]:
    ASYNC_SESSION = async_sessionmaker(ENGINE, expire_on_commit=False)
    async with ASYNC_SESSION() as session:
        yield session


async def get_redis_connection() -> Redis:
    redis_connection = await from_url(
        "redis://{}:{}".format(
            appconfig.base_settings_redis.REDIS_HOST,
            appconfig.base_settings_redis.REDIS_PORT,
        ),
        decode_responses=appconfig.base_settings_redis.REDIS_DECODE_RESPONSES,
    )
    return redis_connection


async def get_user_db(session: AsyncSession = Depends(get_async_session)):
    yield SQLAlchemyUserDatabase(session, models.User)
