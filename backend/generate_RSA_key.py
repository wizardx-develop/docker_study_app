from Crypto.PublicKey import RSA
from app.appconfig import secrets

SECRET_CODE = secrets.RSA_SECRET_CODE
key = RSA.generate(2048)
encrypted_key = key.export_key(passphrase=SECRET_CODE, pkcs=8,
                              protection="scryptAndAES128-CBC",
                              prot_params={'iteration_count':131072})

with open("rsa_key.bin", "wb") as f:
    f.write(encrypted_key)

with open("rsa_key.pem", "wb") as f:
    f.write(key.publickey().export_key())